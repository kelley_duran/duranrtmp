package com.kelleyduran.convortmp;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import org.bytedeco.javacpp.avcodec;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameRecorder;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by kelleyduran on 6/27/16.
 *
 */
public class RecordActivity extends Activity implements View.OnClickListener {

    // logging
    private final static String CLASS_LABEL = "RecordActivity";
    private final static String LOG_TAG = CLASS_LABEL;

    // recorder
    private FFmpegFrameRecorder recorder;
    private boolean recording = false;
    final private static int GOP_LENGTH_IN_FRAMES = 60;
    private int FRAME_RATE = 30;

    // keep track of current frame
    private int frameNumber = 0;

    private int imageWidth = 320;
    private int imageHeight = 240;

    // ui
    private Button btnRecorderControl;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.activity_record);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        initView();
    }

    private void initView() {
         /* get size of screen */
        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

        Point point = new Point();
        display.getSize(point);
        int screenWidth = point.x;
        int screenHeight = point.y;

        RelativeLayout.LayoutParams layoutParam = null;
        LayoutInflater myInflate = null;
        myInflate = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        RelativeLayout topLayout = new RelativeLayout(this);
        setContentView(topLayout);

        LinearLayout preViewLayout = (LinearLayout) myInflate.inflate(R.layout.activity_record, null);
        layoutParam = new RelativeLayout.LayoutParams(screenWidth, screenHeight);
        topLayout.addView(preViewLayout, layoutParam);

        /* add control button: start and stop */
        btnRecorderControl = (Button) findViewById(R.id.recorder_control);
        btnRecorderControl.setText(getString(R.string.start));
        btnRecorderControl.setOnClickListener(this);

        /*calculations to get the layout parameters that will
         decide the position of the video preview box */
        int bg_screen_width = 700;
        int bg_width = 1123;
        int display_width_d = (int) (1.0 * bg_screen_width * screenWidth / bg_width);
        int bg_screen_height = 500;
        int bg_height = 715;
        int display_height_d = (int) (1.0 * bg_screen_height * screenHeight / bg_height);
        int prev_rw, prev_rh;
        int live_width = 640;
        int live_height = 480;
        if(1.0 * display_width_d / display_height_d > 1.0 * live_width / live_height) {
            prev_rh = display_height_d;
            prev_rw = (int) (1.0 * display_height_d * live_width / live_height);
        } else {
            prev_rw = display_width_d;
            prev_rh = (int) (1.0 * display_width_d * live_height / live_width);
        }
        layoutParam = new RelativeLayout.LayoutParams(prev_rw, prev_rh);
        int bg_screen_by = 128;
        layoutParam.topMargin = (int) (1.0 * bg_screen_by * screenHeight / bg_height);
        int bg_screen_bx = 232;
        layoutParam.leftMargin = (int) (1.0 * bg_screen_bx * screenWidth / bg_width);


        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();

        // we need to get the camera facing the user (assuming front camera)
        // in a more advanced app we would ask user which camera.
        int frontCameraIndex = 0;
        int cameraCount = Camera.getNumberOfCameras();
        for(int i = 0; i < cameraCount; i++) {
            Camera.getCameraInfo(i, cameraInfo);
            if(cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT){
                frontCameraIndex = i;
            }
        }

        Camera cameraDevice = Camera.open(frontCameraIndex);
        Log.i(LOG_TAG, "cameara open");
        CameraView cameraView = new CameraView(this, cameraDevice);

        // add camera view to the layout.
        topLayout.addView(cameraView, layoutParam);

    }

    @Override
    public void onClick(View v) {
        if(!recording) {
            initRecorder();
            Log.w(LOG_TAG, "Start Button Pushed");
            btnRecorderControl.setText(getString(R.string.stop));
            recording = true;
        } else {
            Log.w(LOG_TAG, "Stop Button Pushed");
            recording = false;
            btnRecorderControl.setText(R.string.start);
        }
    }


    private void initRecorder() {

        //TODO debug this to improve video quality

       recorder = new FFmpegFrameRecorder(
                "rtmp://52.72.21.166:1935/live/kelleyi",
                imageWidth, imageHeight, 0);

        recorder.setInterleaved(true);
        recorder.setVideoOption("tune", "zerolatency");
        recorder.setVideoOption("preset", "ultrafast");

        // Constant Rate Factor (see: https://trac.ffmpeg.org/wiki/Encode/H.264)
        recorder.setVideoOption("crf", "28");

        // 2000 kb/s, reasonable "sane" area for 720
        recorder.setVideoBitrate(2000000);
        recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264);
        recorder.setFormat("flv");
        // FPS (frames per second)
        recorder.setFrameRate(FRAME_RATE);
        // Key frame interval, in our case every 2 seconds -> 30 (fps) * 2 = 60
        // (gop length)
        recorder.setGopSize(GOP_LENGTH_IN_FRAMES);

        try {
            recorder.start();

        } catch (FrameRecorder.Exception e) {
            // should handle any rtmp errors here if the recorder cannot connect to the endpoint
            e.printStackTrace();
        }
    }


    class CameraView extends SurfaceView implements SurfaceHolder.Callback, Camera.PreviewCallback {

        private SurfaceHolder mHolder;
        private Camera mCamera;

        public CameraView(Context context, Camera camera) {
            super(context);
            mCamera = camera;
            mHolder = getHolder();
            mHolder.addCallback(CameraView.this);
            mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
            mCamera.setPreviewCallback(CameraView.this);

            mCamera.startPreview();
        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            Log.w("camera", "surface created");
            try {
                mCamera.stopPreview();
                mCamera.setPreviewDisplay(holder);
            } catch (IOException exception) {
                mCamera.release();
                mCamera = null;
            }
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            Camera.Parameters camParams = mCamera.getParameters();
            List<Camera.Size> sizes = camParams.getSupportedPreviewSizes();
            // Sort the list in ascending order
            Collections.sort(sizes, new Comparator<Camera.Size>() {

                public int compare(final Camera.Size a, final Camera.Size b) {
                    return a.width * a.height - b.width * b.height;
                }
            });

            // Pick the first preview size that is equal or bigger, or pick the last (biggest) option if we cannot
            // reach the initial settings of imageWidth/imageHeight.
            for (int i = 0; i < sizes.size(); i++) {
                if ((sizes.get(i).width >= imageWidth && sizes.get(i).height >= imageHeight) || i == sizes.size() - 1) {
                    imageWidth = sizes.get(i).width;
                    imageHeight = sizes.get(i).height;
                    Log.v(LOG_TAG, "Changed to supported resolution: " + imageWidth + "x" + imageHeight);
                    break;
                }
            }
            camParams.setPreviewSize(imageWidth, imageHeight);

            Log.v(LOG_TAG, "Setting imageWidth: " + imageWidth + " imageHeight: " + imageHeight + " FRAME_RATE: " + FRAME_RATE);

            camParams.setPreviewFrameRate(FRAME_RATE);
            Log.v(LOG_TAG, "Preview Framerate: " + camParams.getPreviewFrameRate());

            mCamera.setParameters(camParams);
            mCamera.startPreview();
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            try {
                mHolder.addCallback(null);
                mCamera.setPreviewCallback(null);
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
        }


        @Override
        public void onPreviewFrame(byte[] data, Camera camera) {

            if(recording && recorder != null){
                Log.v(LOG_TAG, "recording...");

                /* have many issues with laggy video. The issue could be here
                 do we need both timestamp and fame number? or only one? */

                //TODO debug this to improve video qualtiy.
                long startTime = System.currentTimeMillis();
                long curTime = 1000 * (System.currentTimeMillis() - startTime);

                Frame frame = new Frame(imageWidth, imageHeight, Frame.DEPTH_UBYTE, 2);
                ((ByteBuffer) frame.image[0].position(0)).put(data);
                recorder.setTimestamp(curTime);
                try {
                    recorder.setFrameNumber(frameNumber);
                    frameNumber++;
                    recorder.record(frame);
                    Log.v(LOG_TAG, "recorded...");
                } catch (FrameRecorder.Exception e) {
                    e.printStackTrace();
                }
            }



        }
    }
}

