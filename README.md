# README #

### What is this repository for? ###

* Demo app using rtmp to steam video to a server
* Some source is from samples: https://github.com/bytedeco/sample-projects/blob/master/JavaCV-android-example/app/src/main/java/org/bytedeco/javacv_android_example/record/RecordActivity.java
* this app uses the JavaCV libiary from bytedeco. In the JavaCV, there is a wrapper to the FFMpeg library. 
* this library and all imports make the app size larger.

### How do I get set up? ###

* To build apk use gradle build in the directory that contains build.gradle, this will generate apk with required dependencies. 